{assign var=condensedCss value=true}
{include file="../css.tpl"}

<div class="panel">
    <div class="panel-heading">
        <i class="icon-shopping-cart"></i>
        {l s="Ornavi custom" mod='ornavicustom'}
    </div>
    
    {foreach from=$ornaviProducts item=ornaviProduct key=attribute}
        <div class="ornavi_string_main_container">
            <div style="background-color: #{$ornaviData[$attribute]['color']};" class="ornavi_string"></div>
            <div class="ornavi_string_container">
                {foreach from=$ornaviProduct item=product}
                    <div class="ornavi_on_string">
                        <img class="img-responsive ornavi_product_img ornavi_{$product.features.size}"
                            src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                            alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" 
                            title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                            itemprop="image" /> 
                    </div>
                {/foreach}
        </div>
    </div>
        
        <div class="table-responsive">
            <table class="table" id="orderProducts">
                <thead>
                    <tr>
                        {capture "TaxMethod"}
                                {if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
                                        {l s='tax excluded.' mod='ornavicustom'}
                                {else}
                                        {l s='tax included.' mod='ornavicustom'}
                                {/if}
                        {/capture}
                            <th></th>
                            <th><span class="title_box ">{l s='Product' mod='ornavicustom'}</span></th>
       
                            <th>
                                    <span class="title_box ">{l s='Unit Price' mod='ornavicustom'}</span>
                                    <small class="text-muted">{l s='tax excluded.' mod='ornavicustom'}</small>
                            </th>
                 
                            <th>
                                    <span class="title_box ">{l s='Unit Price' mod='ornavicustom'}</span>
                                    <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                            </th>
                            <th class="text-center"><span class="title_box ">{l s='Qty' mod='ornavicustom'}</span></th>
                           
                            <th class="text-center"><span class="title_box ">{l s='Available quantity' mod='ornavicustom'}</span></th>
                            <th>
                                    <span class="title_box ">{l s='Total' mod='ornavicustom'}</span>
                                    <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                            </th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$ornaviProduct item=product}
                        <tr class="product-line-row">
                            <td>
                                <img class="ornavi-thumbnail" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html':'UTF-8'}" /> 
                            </td>    
                            <td>
                                <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$product['id_product']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}">
                                    <span class="productName">{$product['name']}</span><br />
                                </a>
                            </td>  
                            <td>
                                {displayPrice price=$product.price_tax_exc currency=$currency->id}
                            </td>    
                            <td>
                                {displayPrice price=$product.price currency=$currency->id}
                            </td>   
                            <td class="productQuantity text-center">
                                1
                            </td>
                            <td class="productQuantity text-center">
                                <span class="product_quantity_show{if (int)$product['quantity'] > 1} badge{/if}">{(int)$product['quantity']}</span>
                            </td>   
                            <td>
                                {displayPrice price=$product.price currency=$currency->id}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
    </div>
    <div class="table-responsive ornavi_table_bottom">
        <table class="table">
            <thead>
                <tr>
                    <th>{l s='Feature' mod='ornavicustom'}</th>
                    <th>{l s='Value' mod='ornavicustom'}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{l s='Packaging' mod='ornavicustom'} </td>
                    <td>
                        {if isset(OrnaviCustomConfig::$packaging[$ornaviData[$attribute]['id_packaging']]['img'])}
                            <img src="{OrnaviCustomConfig::$packaging[$ornaviData[$attribute]['id_packaging']]['img']}" alt="card">
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>{l s='Card text' mod='ornavicustom'}</td>
                    <td>
                        {$ornaviData[$attribute]['card_text']}
                    </td>
                </tr>
                <tr>
                    <td>{l s='Color' mod='ornavicustom'}</td>
                    {assign var="color" value="0x`$ornaviData[$attribute]['color']`"}

                    <td>{OrnaviCustomConfig::$color[$color]}</td>
                </tr>
                <tr>
                    <td>{l s='Card' mod='ornavicustom'}</td>
                    <td>
                        {if isset(OrnaviCustomConfig::$card[$ornaviData[$attribute]['id_card']]['img'])}
                            <img src="{OrnaviCustomConfig::$card[$ornaviData[$attribute]['id_card']]['img']}" alt="card">
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>{l s='Size' mod='ornavicustom'}</td>
                    <td>{OrnaviCustomConfig::$size[$ornaviData[$attribute]['size']]}</td>
                </tr>
            </tbody>
            
        </table>
    </div>
    {/foreach}
</div>