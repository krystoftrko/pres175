{extends file='page.tpl'}

{block name='page_content'}
    {assign var="condensedCss" value=true}
    {assign var="constWidth" value=3.5}
    {include file="module:ornavicustom/views/templates/css.tpl"}

    <h1>{l s="Vaše uložené korálky" mod="ornavicustom"}</h1>
    {assign var="productsUrl" value=''}
    {assign var="featuresUrl" value=''}
    {foreach from=$products key=attribute item=productAll}
        {$itemsUrl = ''}
        {$featuresUrl = ''}
        <div class="ornavi_customer_box">
            <div class="ornavi_string_main_container">
                <div style="background-color: #{$ornaviData[$attribute]['color']};" class="ornavi_string"></div>
                <div class="ornavi_string_container">

                    {foreach from=$productAll item=product}
                        {$featuresUrl = "color=0x`$ornaviData[$attribute]['color']`&size=`$ornaviData[$attribute]['size']`&first_prod=`$ornaviData[$attribute]['first_product']`"}
                        {if $itemsUrl}
                            {$itemsUrl = "`$itemsUrl`-`$product.id_product`"}
                        {else}
                            {$itemsUrl = $product.id_product}
                        {/if}

                        <div class="ornavi_on_string">
                            <img class="img-responsive ornavi_product_img ornavi_{$product.features.size}"
                                src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                                alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" 
                                title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                                itemprop="image" /> 
                        </div>
                    {/foreach}
                </div>
            </div>

            <a target="_blank" href="{$productUrl}#{$featuresUrl}&ornavi_products={$itemsUrl}" class="btn btn-info btn-sm ornavi_customer_show">Ukázat</a>
            <a href="{$selfUrl}&ornavi_action=delete_saved&ornavi_product_attribute={$attribute}" class="btn btn-danger btn-sm ornavi_customer_delete">Smazat</a>
        </div>
    {/foreach}
{/block}