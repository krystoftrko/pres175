{function name="ornaviProductColor" features = []}
    {foreach from=$features item=feature}
        {if isset($feature.name) && $feature.name == OrnaviCustomConfig::CONFIG_FILTER_VALUE_COLOR}
            {$feature.value}
        {/if}
    {/foreach}
{/function}

<div id="ornavi_step1">
    <div class="ornavi_step1_wrap">
        <h1>Vítejte v Ornavi creatoru</h1>
        <p>Zde je místo pro Vaši kreativitu a fantazii. Díky pár jednoduchým krokům si můžete vytvořit svůj originální náramek. Začněte výběrem velikosti náramku. V případě, že si nejste jisti, jak správně zápěstí změřit využijte nápovědu uvedenou níže. Poté zvolte barvu návlekového materiálu a pokračujte k výběru korálků. Jdeme na to! </p>
        
        <table>
            <tr>
                <td>
                    <label for="ornavi_size">Velikost náramku</label>
                </td>
                <td>
                    <select class="ornavi_select" id="ornavi_size">
                        {foreach from=$size key=value item=name}
                            <option value="{$value}">{$name}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="ornavi_color">Barva lycry</label>
                </td>
                <td>
                    <select class="ornavi_select" id="ornavi_color">
                        {foreach from=$color key=value item=name}
                            <option value="{$value}">{$name}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="ornavi_first_product">Barva korálku ornavi</label>
                </td>
                <td>
                    <select class="ornavi_select" id="ornavi_first_prod">
                        {foreach from=$firstProducts key=value item=firstProduct}
                            <option value="{$firstProduct.id_product}">
                                {ornaviProductColor features=$firstProduct.features}
                            </option>
                        {/foreach}
                    </select>
                </td>
            </tr>
        </table>
        <div class="step1_foot">
            <a href="{$cmsHelpSize}" target="_blank"><i class="icon-i">i</i> Jak správně měřit zápěstí</a>
            <button id="step1_continue" type="button">POKRAČOVAT <img src="{$moduleRelPath}views/img/arrow1.png" alt=">"></button>
        </div>
    </div>
</div>