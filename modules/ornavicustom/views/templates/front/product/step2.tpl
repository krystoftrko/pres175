{function name="ornaviProductAttr" features = []}
    {foreach from=$features item=feature}
        {if isset($feature.name, $feature.value)}
            {$feature.name} = "{$feature.value}"
        {/if}
    {/foreach}
{/function}

<div id="ornavi_custom">
    <span class="ornavi_head">
         <span><a href="#" id="step1_back">Výběr velikosti a barvy</a></span><span><a href="{$cmsHelpLink}" target="_blank">Nápověda</a></span><span><a href="{$cmsHelpSize}" target="_blank">Správná velikost náramku</a></span><span><a id="ornavi_share" href="#">Sdílejte svůj náramek</a></span>{if $loggedIn}<span><a id="ornavi_save" href="{$selfUrl}">Uložit a dokončit později</a></span>{/if}
    </span>

    <div class="ornavi_fav_head">
        <div class="ornavi_fav_top_head">
            Vaše <img class="ornavi_heart" src="{$moduleRelPath}views/img/heart.png" alt="♥"> korálky <a href="#choose_fav_modal" class="choose_fav ornavi_box">Vyberte zde</a>
        </div>

        <div class="ornavi_fav_head_box">
            {if !$loggedIn}
                <span class="ornavi_comment"><a href="/?controller=authentication">Chcete-li uložit oblíbené korálky pro Váš přístí nákup, zaregistrujte se zde</a></span>
            {/if}
            <div class="ornavi_arrow_fav_box">
                <div class="ornavi_arrow arrow_fav_left"><i></i></div>
                <div class="ornavi_arrow arrow_fav_right"><i></i></div>
             </div>
        </div>

        <div class="ornavi_product_main_container">
            <div class="ornavi_product_container_blankbox blankbox_left"></div>
            <div class="ornavi_fav_container">
                {for $i=1 to 7}
                    <div><div class="ornavi_product_blank"><span class="ornavi_shadow_dot"></span></div></div>
                {/for}
            </div>
         <div class="ornavi_product_container_blankbox blankbox_right"></div>
        </div>
    </div>

    <div class="ornavi_info">
        <span class="ornavi_box_right ornavi_box">
            DÉLKA: <span id="ornavi_length">0</span>cm
        </span>

        <span class="ornavi_length_warning">
            Náramek je příliš krátký
        </span>

        <span class="ornavi_box_left ornavi_box">
            CENA: <span id="ornavi_price">0</span>{$currencySign}
        </span>
    </div>

    <div class="ornavi_string_main_container">
        <div id="ornavi_string"></div>
        <div class="ornavi_string_container">
            <div class="ornavi_on_string ornavi_on_string_first">
                {foreach from=$firstProducts item='firstProduct'}
                    <img class = "ornavi_{$firstProduct.features.size}"
                        {ornaviProductAttr features=$firstProduct.features}
                        class="img-responsive ornavi_product_img"
                        data-price = "{$firstProduct.price_without_reduction}"
                        data-id_product = "{$firstProduct.id_product}"
                        data-length = "{$firstProduct.features.size}"
                        src="{$link->getImageLink($firstProduct.link_rewrite, $firstProduct.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                        alt="{if !empty($firstProduct.legend)}{$firstProduct.legend|escape:'html':'UTF-8'}{else}{$firstProduct.name|escape:'html':'UTF-8'}{/if}" 
                        title="{if !empty($firstProduct.legend)}{$firstProduct.legend|escape:'html':'UTF-8'}{else}{$firstProduct.name|escape:'html':'UTF-8'}{/if}"
                        itemprop="image" /> 
                {/foreach}
            </div>
        </div>
    </div>

    <div class="ornavi_product_menu">
        <div class="ornavi_filter"> 
            <div data-filter_name="{$mainFilterName}" class="ornavi_level_main">
                {foreach from=$mainFilterValues item=filterVal}
                    <span data-filter_value="{$filterVal}" class="ornavi_attr"><span>{$filterVal}</span></span>
                {/foreach}
                <a class="ornavi_filter_modal ornavi_attr" href="#ornavi_filter_modal_click"><span>Filtr</span></a>
            </div>
        </div>
    
        <div class="ornavi_arrow_box">
            <div class="ornavi_arrow arrow_left"><i></i></div>
            <div class="ornavi_arrow arrow_right"><i></i></div>
        </div>
    </div>
    <div class="ornavi_product_main_container">
        <div class="ornavi_product_container_blankbox blankbox_left"></div>
        <div class="ornavi_product_container">
            {foreach from=$products item=product}
                {if $product.quantity < 1}
                    {continue}
                {/if}
            
                <div class="ornavi_product" {ornaviProductAttr features=$product.features} >
                    <div class="ornavi_name">
                        {$product.name}
                        <span class="ornavi_fav add"></span>
                    </div>

                    <img class="img-responsive ornavi_product_img ornavi_{$product.features.size}"
                        data-price = "{$product.price_without_reduction}"
                        data-id_product = "{$product.id_product}"
                        data-length = "{$product.features.size}"
                        src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                        alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" 
                        title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                        itemprop="image" /> 

                    <div class="ornavi_price">
                        {Tools::convertPrice($product.price)} {$currencySign}
                        {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                                                <span class="old-price product-price">
                                                        {Product::displayWtPrice($product.price_without_reduction)}
                                                </span>
                                                {if $product.specific_prices.reduction_type == 'percentage'}
                                                        <span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
                                                {/if}
                                        {/if}
                        <span>{$product.features.size}mm</span>                
                    </div>        
                </div>
            {/foreach}
        </div>
        <div class="ornavi_product_container_blankbox blankbox_right"></div>
    </div>
    <button id="step2_continue" type="button">POKRAČOVAT</button>
</div>