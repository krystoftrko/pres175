var allObjects = {};

var firstProducts = {};

const POS_COLOR = 0,
      POS_SIZE = 1,
      POS_FIRST_PROD = 2,
      POS_ITEMS = 3,
      POS_FILTER = 5,
      POS_STEP3 = 5;

const STRING_LENG_UNDER_LIM = 1;
      STRING_LENG_ABOVE_LIM = 1;

var texts = {
    'product_length_short': 'Náramek je příliš krátký',
    'product_length_ok': 'Délka vašeho náramku je ideální',
    'product_too_long': 'Náramek je příliš dlouhý',
    'product_too_long_warning': 'Opravdu chcete pokračovat? Náramek je delší než zvolená velikost.',
    'in_fav': 'Produkt již je v oblíbených',
    'max_fav': 'Dosažen maximální počet produktů v oblíbených',
    'empty_submit': 'Navlečte korálky',
    'max_input_chars': 'Dosáhli jste maxima znaků',
    'select_packaging': 'K věnování musíte vybrat i krabičku',
    'select_both_packaging': 'K textu musíte vybrat krabičku a věnování',
    'empty_products': 'Přidejte produkty'
};

$(document).ready(function(){

    customSelect('.ornavi_select');

    /* STEP 2 */

    /**
     * Modals
     */
    $('.ornavi_filter_modal').fancybox({
        fitToView	: false,
    });

    $('.choose_fav').fancybox({
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
    });

    /**
     * Init
     */
    allObjects = $('.ornavi_product');
    allObjects.clone().appendTo($('#choose_fav_products'));

    firstProducts = $('.ornavi_on_string_first img');
    $('.ornavi_on_string_first').children().remove();

    initSlick(2,7, '.ornavi_product_container', '.arrow_left', '.arrow_right');
    initSlick(1,7, '.ornavi_fav_container', '.arrow_fav_left', '.arrow_fav_right');
    
    var stringContainer = $('.ornavi_string_container');

    stringContainer.sortable({
        axis: "x",
        items: '.ornavi_on_string',
        cursor: "move",
        tolerance: "intersect",
        distance: 1,
        stop: function(){
            updateStats();
        }
    });

    stringContainer.disableSelection();
    
    stringContainer.droppable({
        accept: ".ornavi_product_img",
        drop: function(event, ui) {
            addProductAtString($(this), ui.draggable);
        }
    });

    /**
     * Textarea gift counter
     */
    $('textarea[name="present_text"]').on("input", function(){
        var maxlength = $(this).attr("maxlength");
        var currentLength = $(this).val().length;
    
        if( currentLength >= maxlength ){
            $('#card_text_counter').text(texts['max_input_chars']);
        }else{
            $('#card_text_counter').text(currentLength + '/' + maxlength);
        }
    });

    /**
     * Share product
     */
    $('#ornavi_share').click(function(e){
        e.preventDefault();
        
        var urlData = getDataFromUrl();
        
        urlData['items'] = 'ornavi_products='+getProducts('-');
        setDataToUrl(urlData);
        window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURI(location.href), 'Facebook','width=640,height=300');
    });

    /**
     * Delete from string
     */
    stringContainer.on('click', '.ornavi_on_string_delete',function(){
        $(this).parent().remove();
        updateStats();
    });

    /**
     * Add on click
     */
    $('.ornavi_product_container, .ornavi_fav_container').on('click', '.ornavi_product .ornavi_product_img',function(){
        addProductAtString(stringContainer, $(this));
    });
    
    /**
     * Add to fav carousel
     */
    $('.ornavi_product_container, #choose_fav_modal').on('click', '.ornavi_fav.add',function(){
        var container = $('.ornavi_fav_container');

        if(container.slick('getSlick').slideCount > 9){            
            alert(texts['max_fav']);
            return;
        }

        var parentEl = $(this).nthParent(2);

        var idProduct = parentEl.find('img').data('id_product');
        var duplicite = container.find('img[data-id_product="'+idProduct+'"]');

        if(duplicite.length > 0){
            alert(texts['in_fav']);
            return;
        }
        var productEl = parentEl.clone();
        var productElButton = productEl.find('.ornavi_fav');
        productElButton.removeClass('add');
        productElButton.addClass('del');

        container.slick('unslick')

        $('.ornavi_fav_container div .ornavi_product_blank').last().parent().remove();
        container.prepend(productEl);
        initSlick(1,7, '.ornavi_fav_container', '.arrow_fav_left', '.arrow_fav_right');
        $('.ornavi_product img[data-id_product="'+idProduct+'"]').parent().find('.ornavi_fav.add').addClass('added');
        
        var chosenCounter =  $('#ornavi_chosen_fav');
        chosenCounter.html(parseInt(chosenCounter.html(), 10)+1);
    });

    /**
     * Delete from fav carousel
     */
    $('.ornavi_fav_container, #choose_fav_modal').on('click', '.ornavi_fav.del',function(){
        var container = $('.ornavi_fav_container');
        var parentEl = $(this).nthParent(2);
        var idProduct = parentEl.find('img').data('id_product');
        var slickIndex = parentEl.attr('data-slick-index');
        container.slick('slickRemove', slickIndex);
        var slick = container.slick('getSlick');

        if(slick.slideCount < 7){
            container.slick('slickAdd', '<div><div class="ornavi_product_blank"><span class="ornavi_shadow_dot"></span></div></div>');
        }

        $('.ornavi_product img[data-id_product="'+idProduct+'"]').parent().find('.ornavi_fav.add').removeClass('added');

        var chosenCounter =  $('#ornavi_chosen_fav');
        chosenCounter.html(parseInt(chosenCounter.html(), 10)-1);
    });

    /**
     * Save progress
     */
    $('#ornavi_save').click(function(e){
        e.preventDefault();
        
        var products = getProducts('-');

        if(products){
            var urlData = getDataFromUrl();
            var path = $('#ornavi_save').attr('href') + '&products=' + products + '&'+ urlData['color'].substring(1) + '&' + urlData['size'] + '&ornavi_action=save';
            window.location.replace(path);
        } else {
            alert(texts['empty_products']);
        }

    });

    /**
     * Filter handle
     */
    $('.ornavi_attr').click(function(){
        var filterName = $(this).parent().attr('data-filter_name');
        var filterValue = $(this).data('filter_value');
        
        if(!filterName || !filterValue){
            return;
        }
        
        $(this).siblings().children().removeClass('ornavi_box_center');
        
        var urlHash = getDataFromUrl();
        var hash = urlHash['filter'];

        if($(this).children().hasClass('ornavi_box_center')){
            $('#ornavi_filter_modal_click .ornavi_select[name="'+filterName+'"]').val('no');
            $(this).children().removeClass('ornavi_box_center');
            hash = removeHashFilter(hash, filterName);
        }
        else{
            hash = addHashFilter(hash, filterName, filterValue);
        }
        
        setDataToUrl(urlHash);
    });

    $('#ornavi_filter_modal_click .ornavi_select').change(function(){
        var filterName = $(this).attr('name');
        var filterValue = this.value;
        
        if(!filterName || !filterValue){
            return;
        }

        $('.ornavi_box_center').removeClass('ornavi_box_center');

        var urlHash = getDataFromUrl();
        var hash = urlHash['filter'];

        if(filterValue === 'no'){
            hash = removeHashFilter(hash, filterName);
        } else {
            hash = addHashFilter(hash, filterName, filterValue);
        }

        setDataToUrl(urlHash);
    });

    $('#step1_back').click(function(){

        setDataToUrl({
            'color': '',
            'size': '',
            'first_prod': '',
            'items': '',
            'filter': []
        });
        scrollTop();;
    });

    /* STEP 1 */
    $('#step1_continue').click(function(){
        var color = 'color=' + $('#ornavi_color').val();
        var size = 'size=' + $('#ornavi_size').val();
        var first_prod_id = $('#ornavi_first_prod').val();
        var first_prod = 'first_prod=' + first_prod_id;

        setFirstProduct(first_prod_id);

        setDataToUrl({
            'color': color,
            'size': size,
            'first_prod': first_prod,
            'items': '',
            'filter': []
        });
        scrollTop();

        setTimeout(function(){
            $('.ornavi_fav_container').slick('unslick');
            initSlick(1,7, '.ornavi_fav_container', '.arrow_fav_left', '.arrow_fav_right');
          }, 100);
    });

    
    $('#step2_back').click(function(){
        var urldata = getDataFromUrl();
        urldata['step3'] = '';
        setDataToUrl(urldata);
        scrollTop();
    });

    /* STEP 3 */
    $('#step2_continue').click(function(){
        var urldata = getDataFromUrl();

        var maxLen = urldata['size'].split('=')[1];
        var stats = getOnStringStats();

        switch(measureStringLength(stats['length'], maxLen)){
            case 'short':
                alert(texts['product_length_short']);
                break;
            case 'long':
                if(!confirm(texts['product_too_long_warning'])){
                    break;
                }
            default:
                urldata['step3'] = 'step3=true';
                setDataToUrl(urldata);
                scrollTop();
        }
    });
   
    /**
     * Change selected packaging id and add img class
     */
    $('.packaging_button').click(function(){
        var nthChild = $(this).parent().prevAll().length+1;
        var img = $('.ornavi3_packaging_wrap div:nth-child('+nthChild+') img');

        step3ToggleClass(img, '.ornavi3_packaging_wrap', $('.ornavi_submit input[name="packaging_id"]'), $(this).data('id'));
        scrollTop();
    });

    /**
     * Change selected card id and add img class
     */
    $('.card_button').click(function(){
        var img = $(this).prev('img');

        step3ToggleClass(img, '.ornavi3_card_items', $('.ornavi_submit input[name="card_id"]'), $(this).data('id'));
    });

    /**
     * On form submit
     */
    $('form[name="ornavi_custom_to_basket"]').submit(function(e){
        e.preventDefault();
        var products = getProducts();
        var formValues = $(this).serializeArray();

        var packaging = formValues.find(function(e){
            return e.name == 'packaging_id'
        });

        var card = formValues.find(function(e){
            return e.name == 'card_id'
        });

        var cardtext = formValues.find(function(e){
            return e.name == 'card_text'
        });

        if(card.value && !packaging.value){
            alert(texts['select_packaging']);
            return;
        }

        if(cardtext.value && !(card.value && packaging.value)){
            alert(texts['select_both_packaging']);
            return;
        }
        
        if(products){
            $urlData = getDataFromUrl();
            var color = $urlData['color'].split('=')[1];
            var size = $urlData['size'].split('=')[1];

            var postData = {
                'ornavi_custom_products': products,
                'card_id': card.value,
                'packaging_id': packaging.value,
                'card_text': cardtext.value,
                'ornavi_action': 'add_to_cart',
                'color': color,
                'size': size
            };

            $.post(document.URL + '&ajax=true' ,postData, function(data){
                changeQuantity(data.product,data.combination);
            }, "json");

            $('form[name="ornavi_custom_to_basket"]').find('input[type=hidden],textarea').val('');
            $('.ornavi3_selected').removeClass('ornavi3_selected');
            $('.ornavi_string_container').children().not('.ornavi_on_string_first').remove();
            setDataToUrl({
                'color': '',
                'size': '',
                'first_prod': '',
                'filter': [],
                'step3': ''
            });
        }else{
            alert(texts['empty_submit']);
        }
    });


    $('.ornavi_string_container').on('mouseenter', '.ornavi_on_string', function(){
        $(this).children('.ornavi_on_string_delete').css('display', 'inline');
    });

    $('.ornavi_string_container').on('mouseleave', '.ornavi_on_string', function(){
        $(this).children('.ornavi_on_string_delete').css('display', 'none');
    });

    /**
     * Redirect to saved
     * For use in future
     */
    /*
    $('#module-ornavicustom-product').on('click','button[data-dismiss="modal"]',function(){
        
    });*/

    var urldata = getDataFromUrl();

    initProductOnStep2(urldata);

    /**
     * When hash change value
     */
    $(window).on('hashchange', function(){
        var urldata = getDataFromUrl();
        initSteps(urldata);
    });

    initSteps(urldata);
    updateStats(urldata);
});

function setFirstProduct(firstProdId){
    $('.ornavi_on_string_first img').remove();
    firstProducts.each(function(){
        if($(this).data('id_product') == firstProdId){
            $(this).clone().appendTo($('.ornavi_on_string_first'));
        }
    });
    if($('.ornavi_on_string_first img').length < 1){
        setDataToUrl({
            'color': '',
            'size': '',
            'first_prod': '',
            'items': '',
            'filter': []
        });
        scrollTop();
    }
}

function scrollTop(){
    $('html, body').animate({
        scrollTop: $('#content').offset().top
    }, 500);
}

function step3ToggleClass(img, wrapClass, input, id){
    if(img.hasClass('ornavi3_selected')) {
        img.removeClass('ornavi3_selected');
        input.val('');
    } else {
        $(wrapClass+' .ornavi3_selected').removeClass('ornavi3_selected');
        img.addClass('ornavi3_selected');
        input.val(id);
    }
}

function initProductOnStep2(urldata){
    var items = urldata['items'].split('=');

    if (typeof items[1] === 'string' ) {
        var stringContainer = $('.ornavi_string_container');

        items[1].split('-').forEach(function(currId){
            var currImg = allObjects.find('img[data-id_product='+currId+']');
            addProductAtString(stringContainer, currImg);
        });
    }
}

function initSteps(urldata){
    var color = urldata['color'].split('=');
    var size = urldata['size'].split('=');
    var first_prod = urldata['first_prod'].split('=');
    var step3 = urldata['step3'].split('=');

    if(typeof color[1] !== "undefined" && typeof size[1] !== "undefined" && typeof first_prod[1] !== "undefined" && $.isNumeric(color[1]) && $.isNumeric(first_prod[1]) && $.isNumeric(size[1]) && color[0] == '#color' && size[0] == 'size'){
        $('#ornavi_step1').hide();
        $('#ornavi_string').css('background-color', '#'+color[1].replace('0x',''));
        setFirstProduct(first_prod[1]);

        if (typeof step3[1] !== 'undefined' && step3[1] == 'true') {
            if (productsAddedByUser()) {
                $('#ornavi_custom').hide();
                $('#ornavi_step3').show();
            } else {
                $('#ornavi_step3').hide();
                $('#ornavi_custom').show();

                alert(texts['empty_products']);

                urldata['step3'] = '';
                setDataToUrl(urldata);
            }
        } else {
            $('#ornavi_step3').hide();
            $('#ornavi_custom').show();
        }
    } else {
        $('#ornavi_custom').hide();
        $('#ornavi_step3').hide();
        $('#ornavi_step1').show();
    }

    initFilter(urldata);
}

function addProductAtString(droppable, draggable){
    if(!draggable.hasClass('ornavi_on_string')){       
        var newElem = draggable.clone();
        newElem.appendTo(droppable);
        newElem.wrap('<div class="ornavi_on_string"></div>');
        $('<div class="ornavi_on_string_delete"></div>').insertAfter(newElem)

        updateStats();
        droppable.sortable('refresh'); 

        return newElem;
    }
}

function getDataFromUrl(){
    var hashParams = location.hash.split('&');
    
    if(typeof hashParams[POS_COLOR] === 'undefined' || typeof hashParams[POS_SIZE] === 'undefined' || typeof hashParams[POS_FIRST_PROD] === 'undefined'){
        return {
            'color': '',
            'size': '',
            'first_prod': '',
            'items': '',
            'filter': [],
            'step3': ''
        };
    }

    if(typeof hashParams[POS_FILTER] === 'undefined'){
        hashParams[POS_FILTER] = 'ornavi_filter';
    }

    if(typeof hashParams[POS_ITEMS] === 'undefined'){
        hashParams[POS_ITEMS] = 'ornavi_products';
    }

    var hash = hashParams[POS_FILTER].split('-');

    return {
        'color': hashParams[POS_COLOR],
        'size': hashParams[POS_SIZE],
        'first_prod': hashParams[POS_FIRST_PROD],
        'items': hashParams[POS_ITEMS],
        'filter': hash,
        'step3': typeof hashParams[POS_STEP3] !== 'undefined'?hashParams[POS_STEP3]:''
    };
}

function setDataToUrl(hashParams){
    hashParams['filter'] = hashParams['filter'].join('-');

    var data = Object.values(hashParams).filter(function (el) {
        return el != "";
      });

    location.hash = data.join('&');
}

function initSlick(rows, slidesToShow, slickClass, leftArrow, rightArrow){
    var jslick = $(slickClass);
    jslick.slick({
        dots: false,
        infinite: true,
        slidesToShow: slidesToShow,
        slidesToScroll: 1,
        arrows: true,
        rows: rows,
        prevArrow: $(leftArrow),
        nextArrow: $(rightArrow),
        draggable: false,
        variableWidth: false,
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 6,
              }
            },
            {
              breakpoint: 990,
              settings: {
                slidesToShow: 5,
              }
            },
            {
                breakpoint: 768,
                settings: {
                  slidesToShow: 4,
                }
            },
            {
                breakpoint: 560,
                settings: {
                  slidesToShow: 3,
                }
            },
            {
                breakpoint: 440,
                settings: {
                  slidesToShow: 2,
                }
            },
            {
                breakpoint: 300,
                settings: {
                  slidesToShow: 1,
                }
            }  
          ]
    }).mousewheel(function(e) {
        e.preventDefault();
        if (e.deltaY < 0) {
            $(this).slick('slickNext');
          }
          else {
            $(this).slick('slickPrev');
          }
    });

    if(rows > 1){
        $('.ornavi_product_container .slick-slide').each(function(){
            $(this).children().each(function(){
                $(this).addClass('ornavi_product_2_cont');
            });
        });
    }

    $(window).on('resize orientationchange', function() {
        jslick.slick('resize');
      });

    $('*[draggable!=true]','.slick-track').unbind('dragstart');

    var draggableInit = {
        revert: "invalid",
        stack: ".draggable",
        helper: 'clone',
        cursor: "move",
        cursorAt: { top: 10, left: -30 }
    };

    $('.ornavi_product_img:not(.ornavi_on_string .ornavi_product_img)').draggable(draggableInit);
}

function getProducts($det = ' '){
    var products = '';

    var count = 0;

    $('.ornavi_string_container').children('.ornavi_on_string').each(function(){
        var product = $(this).children('img').data('id_product');
        if(!product){
            return false;
        }
        products += product + $det;
        count++;
    });

    if (count > 1) {
        return products.slice(0, -1);
    }

    return '';
}

function productsAddedByUser(){
    return $('.ornavi_string_container').children('.ornavi_on_string').length > 1;
}

function updateStats(urldata = ''){
    if(!urldata){
        urldata = getDataFromUrl();
    }

    var maxLen = urldata['size'].split('=')[1];

    var stats = getOnStringStats();

    $('#ornavi_price').html(Math.round(stats['price'] * 100) / 100);
    $('#ornavi_length').html(stats['length']);

    switch(measureStringLength(stats['length'], maxLen)){
        case 'long':
            $('.ornavi_length_warning').html(texts['product_too_long']);
            break;
        case 'ok':
            $('.ornavi_length_warning').html(texts['product_length_ok']);
            break;
        case 'short':
            $('.ornavi_length_warning').html(texts['product_length_short']);
            break;
    }

    
}

function measureStringLength(current, max){
    var current = parseFloat(current);
    var max = parseFloat(max);

    if(current > max-STRING_LENG_UNDER_LIM && current < max + STRING_LENG_ABOVE_LIM){
        return 'ok';
    }
    
    if(current > max){
        return 'long'
    }

    if(current < max){
       return 'short' 
    }
}

function getOnStringStats(){
    var price = 0;
    var length = 0;

    $('.ornavi_string_container').children('.ornavi_on_string').each(function(){
        var img = $(this).children('img');
        price += img.data('price');
        length += img.data('length');
    });
    
    return {'price': price, 'length': length/10};
}

function filter(urlHash = false){
    if (!urlHash) {
        urlHash = getDataFromUrl();
    }

    var hash = urlHash['filter'];

    if(hash[0] !== 'ornavi_filter'){
        return;
    }
    var slickContainer =  $('.ornavi_product_container');

    slickContainer.slick('unslick');
    
    slickContainer.empty();

    var filterFun = function()
    {
        var allow = true;
        for(var iter = 1; iter < hash.length; iter++){
            var nv = hash[iter].split('=');
            var filterName = decodeURI(nv[0]);
            var filterValue = decodeURI(nv[1]);
            allow &= $(this).attr(filterName) == filterValue;
        }

        return allow;
    };

    var newObj = allObjects.filter(filterFun);
    
    slickContainer.append(newObj);

    var len = newObj.length;

    var rows = len < 7?1:2;
    var slides = len < 7?len:7;

    initSlick(rows, slides, '.ornavi_product_container', '.arrow_left', '.arrow_right');

    optimizeSlickAfter(len);
}

function optimizeSlickAfter(slideCount){
    if(slideCount <= 7){
        $('.ornavi_arrow.arrow_right, .ornavi_arrow.arrow_left').css('display', 'none');
    }
    else{
        $('.ornavi_arrow.arrow_right, .ornavi_arrow.arrow_left').css('display', 'inline-block');
    }

   /* switch(slideCount){
        case 1:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 20, left: -425 });
            break;  
        case 2:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -160 });
            break;
        case 3:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -70 });
            break;
        case 4:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -30 });
            break;
        default:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 20, left: 20 });
    }*/
}

function removeHashFilter(hash, filterName){
    for(var iter = 1; iter < hash.length; iter++){
        var nv = hash[iter].split('=');

        if(nv[0] === filterName){
            hash.splice(iter, 1);
        }
    }

    return hash;
}

function addHashFilter(hash, filterName, filterValue){
    var addItem = true;

    for(var iter = 1; iter < hash.length; iter++){
        var nv = hash[iter].split('=');

        if(nv[0] === filterName){
            nv[1] = filterValue;
            addItem = false;
        }
        hash[iter] = nv.join('=');
    }

    if(addItem){
        hash[hash.length] = filterName+'='+filterValue
    } 

    return hash;
}

function initFilter(urldata){
    var hash = urldata['filter'];

    if(hash[0] == 'ornavi_filter'){
        for(var iter = 1; iter < hash.length; iter++){
            var nv = hash[iter].split('=');
            var filterName = decodeURI(nv[0]);
            var filterValue = decodeURI(nv[1]);
            
            var filterLevel = $('div[data-filter_name="'+filterName+'"]');
            var filterElem = filterLevel.children('span[data-filter_value="'+filterValue+'"]');
            filterElem.children().addClass('ornavi_box_center');

            $('#ornavi_filter_modal_click .ornavi_select[name="'+filterName+'"]').val(filterValue);
        }
    }

    filter();
}

function changeQuantity(id, id_attribute){
    var actionURL = '/index.php?controller=cart';

    var query = {
        token: prestashop.static_token,
        id_product: id,
        id_customization: 0,
        ipa: id_attribute,
        add: 1,
        qty: 1,
        action: 'update'
    }

    $.post(actionURL, query, null, 'json').then(function (resp) {
        prestashop.emit('updateCart', {
                reason: $.extend({}, resp, {linkAction: 'add-to-cart'})
            });
    }).fail(function (resp) {
        prestashop.emit('handleError', { eventType: 'addProductToCart', resp: resp });
    });
}

$.fn.nthParent = function(n){
    var p = this;
    for(var i=0;i<n;i++)
        p = p.parent();
    return p;
}