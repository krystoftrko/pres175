<?php

class OrnaviCustomProductModuleFrontController extends ModuleFrontController
{
    
    /** @var OrnaviCustomProduct */
    public $product;

    /**
     * Set controller
     */
    public function __construct()
    {
        parent::__construct();
        $this->product = $this->module->ornaviCustomProduct;
    }
    
    /**
     * Display controller
     *
     * @return void
     */
    public function initContent()
    {
        parent::initContent();

        $products = $this->getFirstAllProducts();

        $this->context->smarty->assign([
            'products' => $products['all'],
            'firstProducts' => $products['first'],
            'link' => $this->context->link,
            'moduleRelPath' => '/modules/'.$this->module->name.'/',
            'filterValues' => OrnaviCustomConfig::$filterValues,
            'filterValuesName' => OrnaviCustomConfig::$filterValuesName,
            'mainFilterValues' => OrnaviCustomConfig::$filterValues[OrnaviCustomConfig::CONFIG_FILTER_VALUE_TYPE],
            'mainFilterName' => OrnaviCustomConfig::CONFIG_FILTER_VALUE_TYPE,
            'currencySign' => $this->context->currency->sign,
            'selfUrl' => $this->context->link->getModuleLink($this->module->name, 'product'),
            'cmsHelpLink' => $this->context->link->getCMSLink($this->module->configuration(OrnaviCustomConfig::CONFIG_CMS_HELP)),
            'cmsHelpSize' => $this->context->link->getCMSLink($this->module->configuration(OrnaviCustomConfig::CONFIG_CMS_SIZE)),
            'packaging' => OrnaviCustomConfig::$packaging,
            'card' => OrnaviCustomConfig::$card,
            'color' => OrnaviCustomConfig::$color,
            'size' => OrnaviCustomConfig::$size,
            'loggedIn' => $this->context->customer->isLogged()
            ]);

        $this->setTemplate('module:'.$this->module->name.'/views/templates/front/product.tpl');
    }

    /**
     * Get first and all products
     */
    private function getFirstAllProducts()
    {
        $products = $this->product->getAllProducts();

        $firstProducts = [];
        $firstProductIds = $this->module->configuration(OrnaviCustomConfig::CONFIG_FIRST_PRODUCT, null, true);

        foreach ($firstProductIds as $firstProduct) {
            $firstProducts[] = $products[$firstProduct];
            unset($products[$firstProduct]);
        }

        return ['all' => $products, 'first' => $firstProducts];
    }

    /**
     * Adds js and css
     *
     * @return bool
     */
    public function setMedia()
    {
        parent::setMedia();
        $modulePath = _MODULE_DIR_.$this->module->name.'/views/';
        
        $this->addCSS($modulePath.'css/ornavicustomfront.css');
        $this->addCSS($modulePath.'modules/slick/slick.css');
        $this->addCSS($modulePath.'modules/slick/slick-theme.css');
        
        $this->addJquery();
        $this->addJqueryUI('ui.draggable');
        $this->addJqueryUI('ui.droppable');
        $this->addJqueryUI('ui.sortable');
        $this->addjqueryPlugin('fancybox');
        $this->addJS($modulePath.'modules/slick/slick.min.js');
        $this->addJS($modulePath.'modules/jquery.mousewheel.min.js');
        $this->addJS($modulePath.'js/ornavicustomfront.js');
        $this->addJS($modulePath.'modules/custom-select/custom-select.min.js');
    }

    /**
     * Catch POST
     *
     * @return void
     */
    public function postProcess()
    {
        $action = Tools::getValue('ornavi_action');

        if ($this->ajax && $action == 'add_to_cart') {
            $this->addProductToCart();
        } else if ($action == 'save') {
            $this->saveProduct();
        }
    }

    /**
     * Save product after post request
     *
     * @return void
     */
    private function saveProduct()
    {
        $data = $this->getOrnaviGetSave();

        if (!$data || !$data['id_customer']) {
            $url = $this->context->link->getModuleLink($this->module->name, 'product');
            Tools::redirect($url);
        }

        $mainProductId = $this->module->configuration(OrnaviCustom::CONFIG_PRODUCT_ID);
        $this->product->addNewCombination($mainProductId, $data, '-');
        
        $url = $this->context->link->getModuleLink($this->module->name, 'customerAccount');
        Tools::redirect($url);
    }

    /**
     * Return array of post values
     * @return array
     */
    private function getOrnaviGetSave()
    {
        $products = Tools::getValue('products');
        $size = Tools::getValue('size');
        $color = Tools::getValue('color');

        $color = str_replace('0x', '', $color);

        if ($products && ctype_xdigit($color) && is_numeric($size)) {
            return [
                'size' => $size,
                'color' => $color,
                'id_customer' => $this->context->customer->id,
                'products' => $products
            ];
        }

        return [];
    }
        
    /**
     * Add product after ajax post request
     *
     * @return void
     */
    private function addProductToCart()
    {
        $data = $this->getOrnaviPostAddToCart();

        if (!$data) {
            ob_end_clean();
            header('Content-Type: application/json');
            die(json_encode([]));  
        }

        $mainProductId = $this->module->configuration(OrnaviCustom::CONFIG_PRODUCT_ID);
        $combinationId = $this->product->addNewCombination($mainProductId, $data);
        $response = [
            'product' => (int)$mainProductId, 
            'combination' => (int)$combinationId
        ];

        ob_end_clean();
        header('Content-Type: application/json');
        die(json_encode($response));  
    }

    /**
     * Return array of post values
     * @return array
     */
    private function getOrnaviPostAddToCart()
    {
        $products = Tools::getValue('ornavi_custom_products');
        $packagingId = Tools::getValue('packaging_id');
        $cardId = Tools::getValue('card_id');
        $cardText = Tools::getValue('card_text');
        $size = Tools::getValue('size');
        $color = Tools::getValue('color');

        $color = str_replace('0x', '', $color);

        if ($products && $cardText !== false && ctype_xdigit($color) && is_numeric($size)) {
            if (is_numeric($packagingId) && is_numeric($cardId)) {
                return [
                    'packagingId' => $packagingId,
                    'cardId' => $cardId,
                    'cardText' => $cardText,
                    'size' => $size,
                    'color' => $color,
                    'id_customer' => $this->context->customer->id,
                    'products' => $products
                ];
            } else {
                return [
                    'packagingId' => 0,
                    'cardId' => 0,
                    'cardText' => $cardText,
                    'size' => $size,
                    'color' => $color,
                    'id_customer' => $this->context->customer->id,
                    'products' => $products
                ];
            }

            
        }

        return [];
    }
}
