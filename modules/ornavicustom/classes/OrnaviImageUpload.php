<?php

class OrnaviImageUpload {
    
    /** @var $imgW int */
    private $imgW = 0;

    /** @var $imgH int */
    private $imgH = 0;

    /** @var $name string */
    private $name;
    
    /** @var $imgPath string */
    private $imgPath;
    
    public function __construct($name, $w, $h)
    {
        $this->imgW = $w;
        $this->imgH = $h;
        $this->name = $name;
        $this->imgPath = __DIR__.'/../views/img_upload/';
    }
    
    public function uploadImage($file)
    {
        $imageData = @getimagesize($file['tmp_name']);

        if (empty($imageData) || $imageData === false) {
            throw new Exception('No image');
        }

        if (!$this->resizeImage($file["tmp_name"], $this->imgPath.$this->name, $imageData)) {
            throw new Exception('Saving image error');
        }
    }
    
    public function deleteImage()
    {
        $fullName = $this->imgPath.$this->name;
        
        if(is_file($fullName)){
            return @unlink($fullName);
        }
        
        return true;
    }
    
    private function resizeImage($src, $dst, $imageData)
    {
        switch($imageData['mime']){
            case 'image/bmp': 
                $img = imagecreatefromwbmp($src); 
                break;
            
            case 'image/gif': 
                $img = imagecreatefromgif($src); 
                break;
            
            case 'image/jpeg': 
                $img = imagecreatefromjpeg($src); 
                break;
            
            case 'image/png': 
                $img = imagecreatefrompng($src); 
                break;
            
            default : 
                return false;
        }

        $new = $this->resizeImageCrop($img, $imageData[0], $imageData[1], $imageData['mime']);

        $this->deleteImage();
        imagepng($new, $dst);

        return true;
    }
    
    private function resizeImageRatio($image, $w, $h, $type)
    {
        if($w > $h){
            $thumb_w = $this->imgW;
            $thumb_h = $h*($this->imgH/$w);
        }

        if($w < $h){
            $thumb_w = $w*($this->imgW/$h);
            $thumb_h = $this->imgH;
        }

        if($w == $h){
            $thumb_w = $this->imgW;
            $thumb_h = $this->imgH;
        }

        $dst_img = $this->imageTrueColor($thumb_w, $thumb_h, $type);
        imagecopyresampled($dst_img, $image,0,0,0,0,$thumb_w,$thumb_h,$w,$h); 
        
        return $dst_img;
    }
    
    private function resizeImageCrop($image, $w, $h, $type)
    {
        $ratio = $this->imgW / $w;
        $new_w = $this->imgW;
        $new_h = $h * $ratio;
        
        if ($new_h < $this->imgH) {
            $ratio = $this->imgH / $h;
            $new_h = $this->imgH;
            $new_w = $w * $ratio;
        }
        
            $image2 = $this->imageTrueColor($new_w, $new_h, $type);
        imagecopyresampled($image2,$image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);

        if (($new_h != $this->imgH) || ($new_w != $this->imgW)) {
                
            $image3 = $this->imageTrueColor($this->imgW,  $this->imgH, $type);
                    
            if ($new_h > $this->imgH) {
                $extra = $new_h - $this->imgH;
                $x = 0;
                $y = round($extra / 2);
                imagecopyresampled($image3,$image2, 0, 0, $x, $y, $this->imgW, $this->imgH, $this->imgW, $this->imgH);
            } else {
                $extra = $new_w - $this->imgW;
                $x = round($extra / 2);
                $y = 0;
                imagecopyresampled($image3,$image2, 0, 0, $x, $y, $this->imgW, $this->imgH, $this->imgW, $this->imgH);
            }
            imagedestroy($image2);
            return $image3;
        } else {
            return $image2;
        }
    }
    
    private function imageTrueColor($new_w, $new_h, $type)
    {
        $image = imagecreatetruecolor ($new_w, $new_h);
        
        if($type == "image/gif" || $type == "image/png"){
            imagecolortransparent($image, imagecolorallocatealpha($image, 0, 0, 0, 127));
            imagealphablending($image, false);
            imagesavealpha($image, true);
        }
        
        return $image;
    }
}
