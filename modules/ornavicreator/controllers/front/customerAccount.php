<?php

class OrnaviCreatorCustomerAccountModuleFrontController extends ModuleFrontController
{
    /** @var OrnaviCreatorDb */
    public $db;

    /** @var int */
    private $customerId;

    /** @var string */
    private $selfUrl;

    /**
     * Set controller
     */
    public function __construct()
    {
        parent::__construct();

        $this->db = $this->module->db;
        $this->customerId = $this->context->customer->id;

        if ($this->customerId <= 0) {
            Tools::redirect('index.php?controller=authentication?back=my-account');
        }

        $this->selfUrl = $this->context->link->getModuleLink($this->module->name, 'customerAccount');
    }

    /**
     * Display controller
     *
     * @return void
     */
    public function initContent()
    {
        parent::initContent();

        $firstProducts = $this->module->configuration(OrnaviCreatorConfig::CONFIG_FIRST_PRODUCT, null, true);

        $prod = $this->db->getUserProductsByCustomer($this->customerId, $firstProducts);

        $urlDet = preg_match('/\b&\b/', $this->selfUrl) === false?'?':'&';

        $this->context->smarty->assign([
            'products' => $prod['products'],
            'ornaviData' => $prod['data'],
            'selfUrl' => $this->selfUrl.$urlDet,
            'productUrl' => $this->context->link->getModuleLink($this->module->name, 'product')
        ]);

        $this->setTemplate('module:' . $this->module->name . '/views/templates/front/customerAccount.tpl');
    }

    /**
     * Adds js and css
     *
     * @return bool
     */
    public function setMedia()
    {
        parent::setMedia();
        
        $modulePath = _MODULE_DIR_ . $this->module->name . '/views/';

        return $this->addCSS($modulePath . 'css/ornavicreatorcustomer.css');
    }

    /**
     * Catch POST
     *
     * @return void
     */
    public function postProcess()
    {
        $action = Tools::getValue('ornavi_action');
        $id = Tools::getValue('ornavi_product_attribute');

        if ($action == 'delete_saved' && is_numeric($id)) {
            $this->db->deleteUserProducts($this->context->customer->id, $id);
            Tools::redirect($this->selfUrl);
        }
    }
}
