<?php

/**
 * TODO:
 *   Ornavi creator product is orderable from list - main combination
 *   PS_IMAGE_QUALITY - png for porducts images
 *   SHARE - add image
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . '/classes/OrnaviCreatorConfig.php';

class OrnaviCreator extends OrnaviCreatorConfig
{

    /**
     * Module setting
     */
    public function __construct()
    {
        $this->name = 'ornavicreator';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'WeboServis, s.r.o.';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Ornavi Creator');
        $this->description = $this->l('Adds Ornavi Creator.');
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
    }

    /**
     * Install module
     *
     * @return bool
     */
    public function install()
    {
        return parent::install()
            & $this->registerHook('displayHeader')
            & $this->registerHook('displayAdminProductsExtra')
            & $this->registerHook('actionProductSave')
            & $this->registerHook('displayBackOfficeHeader')
            & $this->registerHook('displayAdminOrder')
            & $this->registerHook('actionValidateOrder')
            & $this->registerHook('displayCustomerAccount')
            & $this->db->install()
            & $this->installProduct() // Needs old conf value
            & $this->configuration(self::CONFIG_DELETE_DB, false)
            & $this->configuration(self::CONFIG_DELETE_PRODUCTS, false)
            & Configuration::updateValue('PS_IMAGE_QUALITY', 'png');
    }

    /**
     * Install product, features
     *
     * @return bool
     */
    private function installProduct()
    {
        $deleteProduct = $this->configuration(self::CONFIG_DELETE_PRODUCTS);

        if ($deleteProduct) {
            $productId = $this->ornaviCustomProduct->install();
            $this->configuration(self::CONFIG_FEATURES_ID, $this->ornaviCustomProduct->installFeature());
            return $this->configuration(self::CONFIG_PRODUCT_ID, $productId);
        }

        $productId = $this->configuration(self::CONFIG_PRODUCT_ID);
        $product = new Product($productId);

        if (!$product->id) {
            $this->configuration(self::CONFIG_FEATURES_ID, $this->ornaviCustomProduct->installFeature());
            $productId = $this->ornaviCustomProduct->install();
        }

        return $this->configuration(self::CONFIG_PRODUCT_ID, $productId);
    }

    /**
     * Uninstall module
     *
     * @return bool
     */
    public function uninstall()
    {
        return parent::uninstall()
            & $this->uninstallProduct() //MUST be first because of DB
            & $this->db->uninstall($this->configuration(self::CONFIG_DELETE_DB));
    }

    /**
     * Uninstall product, features
     *
     * @return bool
     */
    private function uninstallProduct()
    {
        $deleteProducts = $this->configuration(self::CONFIG_DELETE_PRODUCTS);

        if ($deleteProducts) {
            $result = true;
            $result &= $this->ornaviCustomProduct->uninstall($this->configuration(self::CONFIG_PRODUCT_ID));
            $result &= $this->configuration(self::CONFIG_PRODUCT_ID, null);
            $result &= $this->ornaviCustomProduct->uninstallFeature($this->configuration(self::CONFIG_FEATURES_ID));
            return $result;
        }

        return true;
    }

    /**
     * Hook displayProductTabContent
     *
     * @param  array $params
     * @return void
     */
    public function hookDisplayHeader($params)
    {
        $controller = $this->context->controller;

        if ($controller && $controller->php_self == 'product') {

            $product = $controller->getProduct();

            if ($product && $product->id == $this->configuration(self::CONFIG_PRODUCT_ID)) {
                Tools::redirect($this->context->link->getModuleLink($this->name, 'product'));
            }
        }
    }

    /**
     * Hook displayAdminProductsExtra
     *
     * @return Smarty_Internal_Template
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $value = 0;

        if (isset($params['id_product']) && (int)$params['id_product']) {
            $value = $this->db->isOrnaviCreator($params['id_product']);
        }

        $this->smarty->assign([
            'value' => $value
        ]);

        return $this->display(__FILE__, 'views/templates/hook/adminProductsExtra.tpl');
    }

    /**
     * Hook actionProductSave
     *
     * @param  array $params
     * @return void
     */
    public function hookActionProductSave($params)
    {
        $forOrnaviCreator = Tools::getValue('for_ornavicreator');
        
        if (($forOrnaviCreator === '1' || $forOrnaviCreator === '0') && $params['id_product'] != $this->configuration(self::CONFIG_PRODUCT_ID)) {
            $this->db->switchOrnaviCreatorProduct($forOrnaviCreator, $params['id_product']);
        }
    }

    /**
     * Hook displayBackOfficeHeader
     * Adds css for backoffice
     *
     * @return void
     */
    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('controller') == 'AdminOrders' && Tools::getValue('id_order')) {
            $this->context->controller->addCSS($this->_path . 'views/css/ornavicreatoradmin.css');
        }
    }

    /**
     * Hook displayAdminOrder
     *
     * @param  array $params
     * @return Smarty_Internal_Template
     */
    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);

        $ornaviProducts = $this->db->getProductsInOrder($order);

        $this->smarty->assign([
            'ornaviProducts' => $ornaviProducts['products'],
            'ornaviData' => $ornaviProducts['data'],
            'order' => $order,
            'currency' => $this->context->currency,
            'product_info' => ''
        ]);

        return $this->display(__FILE__, 'views/templates/hook/adminOrder.tpl');
    }

    /**
     * Hook actionValidateOrder
     *
     * @param  array $params
     * @return void
     */
    public function hookActionValidateOrder($params)
    {
        $products = $params['order']->getProducts();

        foreach ($products as $product) {
            if ($product['product_id'] == $this->configuration(self::CONFIG_PRODUCT_ID)) {
                $productsOrnavi = $this->db->getOrnaviIds($product['product_attribute_id']);

                foreach ($productsOrnavi as $productOrnavi) {
                    StockAvailable::updateQuantity($productOrnavi, null, -1);
                }
            }
        }
    }

    /**
     * Hook displayCustomerAccount
     *
     * @param  array $params
     * @return void
     */
    public function hookDisplayCustomerAccount($params)
    {
        $this->context->smarty->assign([
            'url' => $this->context->link->getModuleLink($this->name, 'customerAccount')
        ]);

        return $this->display(__FILE__, 'views/templates/hook/displayCustomerAccount.tpl');
    }
}
