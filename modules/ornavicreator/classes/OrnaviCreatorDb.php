<?php

class OrnaviCreatorDb
{
    const PRODUCT = 'ornavicreator_products';
    const ATTRIBUTE = 'ornavicreator_attribute';
    const USER_PRODUCT = 'ornavicreator_user_product';
    const PS_PRODUCT = 'product';
    const PS_PRODUCT_SHOP = 'product_shop';
    const PS_PRODUCT_LANG = 'product_lang';
    const PS_IMAGE_SHOP = 'image_shop';

    /** @var $db OrnaviCreatorDb */
    private $db;

    /** @var $langId int */
    private $langId;

    /** @var $shopId int */
    private $shopId;

    /**
     * Set values
     *
     * @param int $lang
     * @param int $shop
     */
    public function __construct($lang, $shop)
    {
        $this->db = Db::getInstance();
        $this->langId = $lang;
        $this->shopId = $shop;
    }

    /**
     * Install database
     *
     * @return bool
     */
    public function install()
    {
        $result = true;

        $result &= $this->db->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . self::PRODUCT . '` (
              `id_product` int NOT NULL
            );
        ');

        $result &= $this->db->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . self::ATTRIBUTE . '` (
                `id_product_attribute` int NOT NULL,
                `id_product` int NOT NULL
            );
        ');

        $result &= $this->db->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . self::USER_PRODUCT . '` (
                `id_product_attribute` int NOT NULL,
                `id_customer` int NOT NULL,
                `id_packaging` int NOT NULL,
                `id_card` int NOT NULL,
                `card_text` varchar(200) NOT NULL,
                `color` varchar(10) NOT NULL,
                `size` int NOT NULL,
                `deleted` tinyint(1) NOT NULL
            );
        ');

        return $result;
    }

    /**
     * Uninstall database
     *
     * @param bool $delete
     * @return bool
     */
    public function uninstall($delete)
    {
        if (!$delete) {
            return true;
        }

        $result = true;

        $result &= $this->db->execute('
            DROP TABLE IF EXISTS `' . _DB_PREFIX_ . self::PRODUCT . '`;
        ');

        $result &= $this->db->execute('
            DROP TABLE IF EXISTS `' . _DB_PREFIX_ . self::ATTRIBUTE . '`;
        ');

        return $result;
    }

    /**
     * Is product in table
     *
     * @param int $id
     * @return bool
     */
    public function isOrnaviCreator($id)
    {
        $q = (new DbQuery())
            ->select('count(*)')
            ->from(self::PRODUCT)
            ->where('id_product = ' . pSQL($id));

        $res = $this->db->getRow($q);
        return (bool)$res['count(*)'];
    }

    /**
     * Add product to table, or delete
     *
     * @param  bool $value
     * @param  int $id
     * @return bool
     */
    public function switchOrnaviCreatorProduct($value, $id)
    {
        if ($value) {
            $result = true;

            $data = [
                'visibility' => 'none',
                'indexed' => 0,
                'available_for_order' => 0,
                'show_price' => 0,
            ];

            $where = 'id_product = ' . pSQL($id);

            $result &= $this->db->update(self::PS_PRODUCT, $data, $where);
            $result &= $this->db->update(self::PS_PRODUCT_SHOP, $data, $where);

            if ($this->isOrnaviCreator($id)) {
                $result &= $this->db->delete(self::PRODUCT, 'id_product = ' . pSQL($id));
            }

            $result &= $this->db->insert(self::PRODUCT, ['id_product' => $id]);

            return $result;
        }
        return $this->db->delete(self::PRODUCT, 'id_product = ' . pSQL($id));
    }

    /**
     * Get all ids from table
     *
     * @return array
     */
    public function getProductIds()
    {
        $q = (new DbQuery())
            ->select('id_product')
            ->from(self::PRODUCT);

        $result = $this->db->executeS($q);

        if (!$result) {
            return [];
        }

        return array_map(function ($item) {
            return $item['id_product'];
        }, $result);
    }

    /**
     * Get ornavi creator products for front template
     *
     * @return array
     */
    public function getProducts()
    {
        $q = $this->ornaviProductSql();
        $result = $this->db->executeS($q);
        return $this->getProductProperties($result);
    }

    /**
     * Get products for select
     *
     * @return array
     */
    public function getProductsList()
    {
        $q = $this->ornaviProductSql();
        $result = $this->db->executeS($q);
        $products = [];

        foreach ($result as $prod) {
            $products[] = [
                'id_product' => $prod['id_product'],
                'name' => $prod['name'] . ' (' . $prod['id_product'] . ')',
                'id_image' => $prod['id_image']
            ];
        }

        return $products;
    }

    /**
     * SQL shortcut
     * 
     * @return DbQuery
     */
    private function ornaviProductSql()
    {
        return (new DbQuery())
            ->select('pr.*, pl.*, image_shop.id_image')
            ->from(self::PRODUCT, 'p')
            ->leftJoin(self::PS_PRODUCT_LANG, 'pl', 'pl.id_product = p.id_product AND pl.id_lang = ' . pSQL($this->langId) . ' AND pl.id_shop = ' . $this->shopId)
            ->leftJoin(self::PS_PRODUCT, 'pr', 'pr.id_product = p.id_product')
            ->leftJoin(self::PS_IMAGE_SHOP, 'image_shop', 'image_shop.id_product = p.id_product AND image_shop.cover=1 AND pl.id_shop = ' . $this->shopId);
    }

    /**
     * Product properties
     *
     * @return array
     */
    private function getProductProperties($result)
    {
        $products = [];

        foreach (Product::getProductsProperties($this->langId, $result) as $product) {
            $products[$product['id_product']] = $product;

            $size = 0;

            foreach ($product['features'] as $feature) {
                if ($feature['name'] == OrnaviCreatorConfig::$featureNames[OrnaviCreatorConfig::CONFIG_FILTER_VALUE_SIZE]) {
                    $size = filter_var($feature['value'], FILTER_SANITIZE_NUMBER_INT);
                }
            }

            $products[$product['id_product']]['features']['size'] = $size;
        }

        return $products;
    }

    /**
     * Product properties
     *
     * @return array
     */
    private function getProductPropertiesAdmin($result)
    {
        $products = [];

        foreach (Product::getProductsProperties($this->langId, $result) as $key => $product) {
            $products[$key] = $product;

            $size = 0;

            foreach ($product['features'] as $feature) {
                if ($feature['name'] == OrnaviCreatorConfig::$featureNames[OrnaviCreatorConfig::CONFIG_FILTER_VALUE_SIZE]) {
                    $size = filter_var($feature['value'], FILTER_SANITIZE_NUMBER_INT);
                }
            }

            $products[$key]['features']['size'] = $size;
        }

        return $products;
    }

    /**
     * Get ornavi creator product for backoffice
     *
     * @param Order $order
     * @return array
     */
    public function getProductsInOrder($order)
    {
        $orderProducts = $order->getProductsDetail();

        $finalProducts = [];
        $ornaviData = [];
        $ornaviproducts = [];

        foreach ($orderProducts as $ornaviProduct) {
            $p = $this->getOrnaviIds($ornaviProduct['product_attribute_id']);

            if ($p) {
                $ornaviproducts[$ornaviProduct['product_attribute_id']] = $p;
            }
        }

        $finalProductOne = [];

        foreach ($ornaviproducts as $attrId => $ornaviProductIds) {
            $finalProductOne = [];

            foreach ($ornaviProductIds as $pId) {
                $q = (new DbQuery())
                    ->select('pr.*, pl.*, image_shop.id_image')
                    ->from(self::PS_PRODUCT, 'pr')
                    ->where('pr.id_product = ' . pSql($pId))
                    ->leftJoin(self::PS_PRODUCT_LANG, 'pl', 'pl.id_product = pr.id_product AND pl.id_lang = ' . pSQL($this->langId) . ' AND pl.id_shop = ' . $this->shopId)
                    ->leftJoin(self::PS_IMAGE_SHOP, 'image_shop', 'image_shop.id_product = pr.id_product AND image_shop.cover=1 AND pl.id_shop = ' . $this->shopId);

                $finalProductOne[] = $this->db->getRow($q);
            }

            $finalProducts[$attrId] = $this->getProductPropertiesAdmin($finalProductOne);
            $ornaviData[$attrId] = $this->getUserProductsByAttribute($attrId);
        }

        return ['products' => $finalProducts, 'data' => $ornaviData];
    }

    /**
     * Add new attribute
     *
     * @param int $attributeId
     * @param array $ornaviProducts
     * @param array $data
     * @return void
     */
    public function addAttribute($attributeId, $ornaviProducts, $data)
    {
        foreach ($ornaviProducts as $ornaviProduct) {
            $this->db->insert(self::ATTRIBUTE, [
                'id_product_attribute' => $attributeId,
                'id_product' => $ornaviProduct->id,
            ]);
        }

        $this->db->insert(self::USER_PRODUCT, [
            'id_product_attribute' => $attributeId,
            'id_packaging' => $data['packagingId'],
            'id_card' => $data['cardId'],
            'card_text' => $data['cardText'],
            'color' => $data['color'],
            'id_customer' => $data['id_customer'],
            'size' => $data['size'],
            'deleted' => 0,
        ]);
    }

    /**
     * Get ids where attribute
     *
     * @param  int $attributeId
     * @return array
     */
    public function getOrnaviIds($attributeId)
    {
        $q = (new DbQuery())
            ->select('id_product')
            ->from(self::ATTRIBUTE)
            ->where('id_product_attribute = ' . pSQL($attributeId));

        $result = $this->db->executeS($q);

        if (!$result) {
            return [];
        }

        return array_map(
            function ($item) {
                return $item['id_product'];
            },
            $result
        );
    }

    /**
     * Get user product by attribute
     *
     * @param  int $id
     * @return array
     */
    public function getUserProductsByAttribute($id)
    {
        $q = (new DbQuery())
            ->select('*')
            ->from(self::USER_PRODUCT)
            ->where('id_product_attribute = ' . pSQL($id));

        return $this->db->getRow($q);
    }

    /**
     * Get user product by user id
     *
     * @param  int $id
     * @return array
     */
    public function getUserProductsByCustomer($id, $firstProducts)
    {
        $q = (new DbQuery())
            ->select('at.*, pr.*, pl.*, image_shop.id_image, up.color, up.size')
            ->from(self::USER_PRODUCT, 'up')
            ->where('id_customer = ' . pSQL($id) . ' AND deleted = 0')
            ->leftJoin(self::ATTRIBUTE, 'at', 'at.id_product_attribute = up.id_product_attribute')
            ->leftJoin(self::PS_PRODUCT_LANG, 'pl', 'pl.id_product = at.id_product AND pl.id_lang = ' . pSQL($this->langId) . ' AND pl.id_shop = ' . $this->shopId)
            ->leftJoin(self::PS_PRODUCT, 'pr', 'pr.id_product = at.id_product')
            ->leftJoin(self::PS_IMAGE_SHOP, 'image_shop', 'image_shop.id_product = at.id_product AND image_shop.cover=1 AND pl.id_shop = ' . $this->shopId);

        $result = $this->db->executeS($q);

        $products = [];
        $ornaviData = [];

        foreach (Product::getProductsProperties($this->langId, $result) as $key => $product) {
            $products[$product['id_product_attribute']][$key] = $product;
            $size = 0;

            foreach ($product['features'] as $feature) {
                if ($feature['name'] == OrnaviCreatorConfig::$featureNames[OrnaviCreatorConfig::CONFIG_FILTER_VALUE_SIZE]) {
                    $size = filter_var($feature['value'], FILTER_SANITIZE_NUMBER_INT);
                }
            }

            $products[$product['id_product_attribute']][$key]['features']['size'] = $size;
            $ornaviData[$product['id_product_attribute']]['color'] = $product['color'];
            $ornaviData[$product['id_product_attribute']]['size'] = $product['size'];

            if (in_array($product['id_product'], $firstProducts)) {
                $ornaviData[$product['id_product_attribute']]['first_product'] = $product['id_product'];
            }
        }

        return ['products' => $products, 'data' => $ornaviData];
    }

    /**
     * Delete user product
     *
     * @param  int $idCustomer
     * @param  int $idProductAttr
     * @return array
     */
    public function deleteUserProducts($idCustomer, $idProductAttr)
    {
        return $this->db->update(self::USER_PRODUCT, ['deleted' => 1], 'id_product_attribute = ' . pSQL($idProductAttr) . ' AND ' . 'id_customer = ' . pSQL($idCustomer));
    }
}
