<?php

require_once __DIR__ . '/OrnaviCreatorDb.php';
require_once __DIR__ . '/OrnaviCreatorProduct.php';
require_once __DIR__ . '/OrnaviImageUpload.php';

class OrnaviCreatorConfig extends Module
{

    const CONFIG_DELETE_DB = 'delete_db';
    const CONFIG_DELETE_PRODUCTS = 'delete_products';
    const CONFIG_PRODUCT_ID = 'product_id';
    const CONFIG_FEATURES_ID = 'features_id';
    const CONFIG_CMS_SIZE = 'cms_size';
    const CONFIG_CMS_HELP = 'cms_help';
    const CONFIG_FIRST_PRODUCT = 'first_product';
    const CONFIG_IMG_STEP3_1 = 'imgstep31';
    const CONFIG_IMG_STEP3_2 = 'imgstep32';
    const CONFIG_IMG_STEP3_3 = 'imgstep33';
    const CONFIG_IMG_STEP3_4 = 'imgstep34';
    const CONFIG_FILTER_VALUE_TYPE = 'ornavi_creator_type';
    const CONFIG_FILTER_VALUE_SIZE = 'ornavi_creator_size';
    const CONFIG_FILTER_VALUE_COLOR = 'ornavi_creator_color';
    const CONFIG_FILTER_VALUE_MATERIAL = 'ornavi_creator_material';

    public static $filterValues = [
        self::CONFIG_FILTER_VALUE_COLOR => [
            'Modrá', 'Zelená', 'Červená', 'Bílá', 'Černá', 'Žlutá'
        ],
        self::CONFIG_FILTER_VALUE_SIZE => [
            '6mm', '8mm', '10mm'
        ],
        self::CONFIG_FILTER_VALUE_TYPE => [
            'Korálky', 'Mezikorálky', 'Středy'
        ],
        self::CONFIG_FILTER_VALUE_MATERIAL => [
            'Skleněné', 'Plastové', 'Kamenné'
        ]
    ];

    public static $filterValuesName = [
        self::CONFIG_FILTER_VALUE_TYPE => 'Typ:',
        self::CONFIG_FILTER_VALUE_SIZE => 'Velikost:',
        self::CONFIG_FILTER_VALUE_COLOR => 'Barva:',
        self::CONFIG_FILTER_VALUE_MATERIAL => 'Druh:',
    ];

    public static $featureNames = [
        self::CONFIG_FILTER_VALUE_TYPE => 'Ornavi Creator: Typ',
        self::CONFIG_FILTER_VALUE_SIZE => 'Ornavi Creator: Velikost',
        self::CONFIG_FILTER_VALUE_COLOR => 'Ornavi Creator: Barva',
        self::CONFIG_FILTER_VALUE_MATERIAL => 'Ornavi Creator: Druh',
    ];

    public static $featureNamesReverse = [
        'Ornavi Creator: Typ' => self::CONFIG_FILTER_VALUE_TYPE,
        'Ornavi Creator: Velikost' => self::CONFIG_FILTER_VALUE_SIZE,
        'Ornavi Creator: Barva' => self::CONFIG_FILTER_VALUE_COLOR,
        'Ornavi Creator: Druh' => self::CONFIG_FILTER_VALUE_MATERIAL,
    ];

    public static $color = [
        '0x000' => 'Černá',
        '0xfff' => 'Bílá',
        //'0x00ff00' => 'Zelená',
        //'0xff0000' => 'Červená',
        //'0x0000ff' => 'Modrá'
    ];

    public static $size = [
        '10' => 'Délka: 10 cm',
        '15' => 'Délka: 15 cm',
        '20' => 'Délka: 20 cm',
    ];

    public static $packaging;
    public static $card;

    /** @var OrnaviCreatorDb */
    public $db;

    /** @var OrnaviCreatorProduct */
    public $ornaviCustomProduct;

    /**
     * Set DB and ornaviCustomProduct
     */
    public function __construct()
    {
        parent::__construct();
        $this->db = new OrnaviCreatorDb($this->context->language->id, $this->context->shop->id);
        $this->ornaviCustomProduct = new OrnaviCreatorProduct($this->db);
        $this->setCardPackaging();
    }

    /**
     * Set card and packaging
     *
     * @return void
     */
    private function setCardPackaging()
    {
        $url = '/modules' . '/' . $this->name . '/views/img_upload/';
        $ver = rand(0, 1000);

        self::$packaging = [
            1 => [
                'id' => 1,
                'img' => $url . 'package1.png?v=' . $ver
            ],
            2 => [
                'id' => 2,
                'img' => $url . 'package2.png?v=' . $ver
            ]
        ];

        self::$card = [
            1 => [
                'id' => 1,
                'img' => $url . 'card1.png?v=' . $ver
            ],
            2 => [
                'id' => 2,
                'img' => $url . 'card2.png?v=' . $ver
            ]
        ];
    }

    /**
     * Get config form content
     *
     * @return string
     */
    public function getContent()
    {
        $output = '';

        $url = $this->context->link->getModuleLink($this->name, 'product');

        $output .= $this->displayConfirmation($this->l('Ornavi Creator URL: ') . ' <a href="' . $url . '">' . $url . '</a>');

        if (Tools::isSubmit('submit' . $this->name)) {
            $deleteDatabase = Tools::getValue(self::CONFIG_DELETE_DB);
            $deleteProducts = Tools::getValue(self::CONFIG_DELETE_PRODUCTS);
            $cmsSize = Tools::getValue(self::CONFIG_CMS_SIZE);
            $cmsHelp = Tools::getValue(self::CONFIG_CMS_HELP);
            $firstProduct = Tools::getValue(self::CONFIG_FIRST_PRODUCT);

            $success = true;

            if ($deleteDatabase === '1' || $deleteDatabase === '0') {
                $success &= $this->configuration(self::CONFIG_DELETE_DB, $deleteDatabase);
            }
            if ($deleteProducts === '1' || $deleteProducts === '0') {
                $success &= $this->configuration(self::CONFIG_DELETE_PRODUCTS, $deleteProducts);
            }
            if (is_numeric($cmsSize)) {
                $success &= $this->configuration(self::CONFIG_CMS_SIZE, $cmsSize);
            }
            if (is_numeric($cmsHelp)) {
                $success &= $this->configuration(self::CONFIG_CMS_HELP, $cmsHelp);
            }
            if (is_array($firstProduct) && $firstProduct) {
                $success &= $this->configuration(self::CONFIG_FIRST_PRODUCT, $firstProduct, true);
            }

            try {
                if ($_FILES[self::CONFIG_IMG_STEP3_1]['error'] === UPLOAD_ERR_OK) {
                    $imageUpload1 = new OrnaviImageUpload('package1.png', 300, 360);
                    $imageUpload1->uploadImage($_FILES[self::CONFIG_IMG_STEP3_1]);
                }

                if ($_FILES[self::CONFIG_IMG_STEP3_2]['error'] === UPLOAD_ERR_OK) {
                    $imageUpload2 = new OrnaviImageUpload('package2.png', 300, 360);
                    $imageUpload2->uploadImage($_FILES[self::CONFIG_IMG_STEP3_2]);
                }

                if ($_FILES[self::CONFIG_IMG_STEP3_3]['error'] === UPLOAD_ERR_OK) {
                    $imageUpload3 = new OrnaviImageUpload('card1.png', 250, 100);
                    $imageUpload3->uploadImage($_FILES[self::CONFIG_IMG_STEP3_3]);
                }

                if ($_FILES[self::CONFIG_IMG_STEP3_4]['error'] === UPLOAD_ERR_OK) {
                    $imageUpload4 = new OrnaviImageUpload('card2.png', 250, 100);
                    $imageUpload4->uploadImage($_FILES[self::CONFIG_IMG_STEP3_4]);
                }
            } catch (Exception $e) {
                $this->displayError($e->getMessage());
                $success = false;
            }

            if ($success) {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            } else {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            }
        }

        return $output . $this->displayForm();
    }

    /**
     * Display config form
     *
     * @return string
     */
    public function displayForm()
    {
        $fields_form = [];

        $cmsPages = CMS::listCms();
        $product = $this->db->getProductsList();

        $fields_form[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'switch',
                    'required' => true,
                    'label' => $this->l('Delete database with uninstall'),
                    'name' => self::CONFIG_DELETE_DB,
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ]
                    ]
                ],
                [
                    'type' => 'switch',
                    'required' => true,
                    'label' => $this->l('Delete products with uninstall'),
                    'name' => self::CONFIG_DELETE_PRODUCTS,
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ]
                    ]
                ],
                [
                    'type' => 'select',
                    'required' => true,
                    'label' => $this->l('CMS Hint for corrent product size'),
                    'name' => self::CONFIG_CMS_SIZE,
                    'options' => [
                        'query' => $cmsPages,
                        'id' => 'id_cms',
                        'name' => 'meta_title'
                    ]
                ],
                [
                    'type' => 'select',
                    'required' => true,
                    'label' => $this->l('CMS Help'),
                    'name' => self::CONFIG_CMS_HELP,
                    'options' => [
                        'query' => $cmsPages,
                        'id' => 'id_cms',
                        'name' => 'meta_title'
                    ]
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Ornavi Products'),
                    'desc' => $this->l('Please select a Ornavi Special Product'),
                    'name' => self::CONFIG_FIRST_PRODUCT,
                    'class' => 'chosen',
                    'multiple' => true,
                    'options' => [
                        'query' => $product,
                        'id' => 'id_product',
                        'name' => 'name'
                    ]
                ],
                [
                    'type' => 'html',
                    'html_content' => '<img class="img-responsive" style="max-height: 80px;" src="' . self::$packaging[1]['img'] . '">',
                    'name' => ''
                ],
                [
                    'type' => 'file',
                    'label' => $this->l('Image of package 1'),
                    'name' =>  self::CONFIG_IMG_STEP3_1
                ],
                [
                    'type' => 'html',
                    'html_content' => '<img class="img-responsive" style="max-height: 80px;" src="' . self::$packaging[2]['img'] . '">',
                    'name' => ''
                ],
                [
                    'type' => 'file',
                    'label' => $this->l('Image of package 2'),
                    'name' =>  self::CONFIG_IMG_STEP3_2
                ],
                [
                    'type' => 'html',
                    'html_content' => '<img class="img-responsive" style="max-height: 80px;" src="' . self::$card[1]['img'] . '">',
                    'name' => ''
                ],
                [
                    'type' => 'file',
                    'label' => $this->l('Image of wish card 1'),
                    'name' =>  self::CONFIG_IMG_STEP3_3
                ],
                [
                    'type' => 'html',
                    'html_content' => '<img class="img-responsive" style="max-height: 80px;" src="' . self::$card[2]['img'] . '">',
                    'name' => ''
                ],
                [
                    'type' => 'file',
                    'label' => $this->l('Image of wish card 2'),
                    'name' =>  self::CONFIG_IMG_STEP3_4
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit' . $this->name;

        $helper->fields_value[self::CONFIG_DELETE_DB] = $this->configuration(self::CONFIG_DELETE_DB);
        $helper->fields_value[self::CONFIG_DELETE_PRODUCTS] = $this->configuration(self::CONFIG_DELETE_PRODUCTS);
        $helper->fields_value[self::CONFIG_CMS_SIZE] = $this->configuration(self::CONFIG_CMS_SIZE);
        $helper->fields_value[self::CONFIG_CMS_HELP] = $this->configuration(self::CONFIG_CMS_HELP);
        $helper->fields_value[self::CONFIG_FIRST_PRODUCT . '[]'] = $this->configuration(self::CONFIG_FIRST_PRODUCT, null, true);

        return $helper->generateForm($fields_form);
    }

    /**
     * Update or get configuration
     *
     * @param  string $conf configuration name
     * @param  string|array $set set configuration value
     * @return string
     */
    public function configuration($conf, $set = null, $array = false)
    {
        if (is_null($set)) {

            $value = Configuration::get($this->name . '_' . $conf);
            if ($array) {
                return explode(',', $value);
            }

            return $value;
        } else {
            if ($array) {
                return Configuration::updateValue($this->name . '_' . $conf, implode(',', $set));
            }

            return Configuration::updateValue($this->name . '_' . $conf, $set);
        }
    }
}
