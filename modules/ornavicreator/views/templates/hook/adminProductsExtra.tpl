<div id="product-ModuleOrnaviCreator" class="panel product-tab">
    <input type="hidden" name="submitted_tabs[]" value="ModuleOrnaviCreator" />
    <h3>{l s='Settings' mod='ornavicreator'}</h3>
        
        <label class="control-label">
            {l s='Product is for ornavi creator' mod='ornavicreator'}
        </label>
        <div class="tab-content">
            <div class="col-lg-3">
                <input type="radio" name="for_ornavicreator" id="for_ornavicreator" value="1" {if $value==1}checked="checked"{/if}>
                <label for="for_ornavicreator" class="radioCheck">
                    {l s='yes'}
                </label>
            </div>
                    
            <div class="col-lg-3">
                <input type="radio" name="for_ornavicreator" id="for_ornavicreator_off" value="0" {if $value==0}checked="checked"{/if}>
                <label for="for_ornavicreator_off" class="radioCheck">
                    {l s='no'}
                </label>
            </div>
        </div>
</div>
