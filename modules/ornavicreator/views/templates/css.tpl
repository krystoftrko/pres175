<style>
    {if empty($constWidth)}
        {assign var="constWidth" value=4}
    {/if}

    {assign var="filterSize" value=OrnaviCreatorConfig::$filterValues[OrnaviCreatorConfig::CONFIG_FILTER_VALUE_SIZE]}
    {assign var="finalHeight" value=(int)filter_var(end($filterSize), FILTER_SANITIZE_NUMBER_INT)*$constWidth}

    {foreach from=$filterSize item="val"}
        {assign var="width" value=(int)filter_var($val, FILTER_SANITIZE_NUMBER_INT)}
        {assign var="top" value=($finalHeight - $width*$constWidth)/2}

        {if empty($condensedCss)}
            .ornavi_on_string img.ornavi_{$width} + .ornavi_on_string_delete{
                bottom: 110%;
                right: 35%;
            }
        {/if}

        {if empty($condensedCss)}.ornavi_product_img.ui-draggable-dragging.ornavi_{$width},{/if} .ornavi_on_string img.ornavi_{$width}{
            width: {$width*$constWidth}px;
        }
    {/foreach}

    .ornavi_string_container{
        height: {$finalHeight}px;
    }

</style>