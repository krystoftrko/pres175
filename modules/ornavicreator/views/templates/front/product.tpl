{extends file='page.tpl'}

{block name='page_content'}
    {assign var="path" value="module:ornavicreator/views/templates/front/product/"}

    {include file="module:ornavicreator/views/templates/css.tpl"}
    {include file="{$path}step1.tpl"}
    {include file="{$path}modals.tpl"}
    {include file="{$path}step2.tpl"}
    {include file="{$path}step3.tpl"}
{/block}
