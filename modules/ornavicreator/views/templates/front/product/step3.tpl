<div id="ornavi_step3">
    <div class="ornavi_step3_wrap">
        <h1>Dárkové balení a věnování</h1>
        <p>Nejpodstatnější část práce s creatorem je za Vámi. Věříme, že jste popustili uzdu své fantazii a sestavili si originální náramek. V tomto kroku si můžete svou ozdobu zabalit do dárkové krabičky. V případě, že ji hodláte někomu věnovat, připojte kartičku s vlastním textem. Tato služba je pro Vás zcela ZDARMA.</p>
        
        <form name="ornavi_creator_to_basket" action="{$link->getModuleLink('ornavicreator', 'product')}" method="POST">
            <div class="ornavi3_block_container">
                <div class="ornavi3_block">
                    <div class="ornavi3_head">Krabičky</div>
                    <div class="ornavi3_subhead">
                        Vyberte jednu z našich dárkových krabiček. Krabičky jsou vyrobené z tvrdého kartonu, uvnitř s polštářkem v černé barvě. Rozměr 9x9cm.
                    </div>

                    <div class="ornavi3_packaging_wrap">
                        {foreach from=$packaging item=pack}
                            <div class="ornavi3_packaging">
                                <img src="{$pack.img}" alt="Balení 1">
                            </div>
                        {/foreach}
                    </div>
                </div>

                <div class="ornavi3_divider"></div>

                <div class="ornavi3_block">
                    <div class="ornavi3_head">Věnování</div>
                    <div class="ornavi3_subhead">
                        Zvolte barvu kartičky. Do volného pole napište text věnování, případně Váš podpis. Kartička bude vložena do krabičky společně s náramkem. 
                    </div>

                    <div class="ornavi3_card">
                        <div class="ornavi3_card_items">
                            {foreach from=$card item=car}
                                <div>
                                    <img src="{$car.img}" alt="Modrá kartička">
                                    <button type="button" data-id="{$car.id}" class="ornavi_box card_button">Vybrat ></button>
                                </div>
                            {/foreach}
                        </div>

                        <textarea name="card_text" maxlength='90' placeholder="Zde můžete napsat vaše věnování..."></textarea>
                        <div class="card_text_counter_wrap">
                            <span id="card_text_counter">0/90</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ornavi3_block_buttons">
                {foreach from=$packaging item=pack}
                    <div>
                        <button type="button" class="ornavi_box packaging_button" data-id="{$pack.id}">Vybrat ></button>
                    </div>
                {/foreach}
            </div>

            <div class="ornavi_submit">
                <a href="{$cmsHelpLink}" target="_blank"><i class="icon-i">i</i> Pořebuji poradit</a>
                <button type="button" id="step2_back">< Zpět k náramku</button>
                <button type="submit" name="submit">POKRAČOVAT <img src="{$moduleRelPath}views/img/arrow1.png" alt=">"></button>
                <input type="hidden" name="packaging_id" value = "">
                <input type="hidden" name="card_id" value = "">
            </div>
        </form>
    </div>
</div>
