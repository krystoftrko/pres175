<div id="choose_fav_modal" style="display: none">
    <div class="choose_fav_header">
        <div class="choose_fav_text">
            <h1>Vyberte své oblíbené korálky</h1>
            <p>V této tabulce si můžete předem vybrat korálky, které použijete pro sestavení svého náramku. Každý korálek si z oblíbených můžete kdykoliv odebrat, nebo přidat nový. Tato funkce (<span>♥</span>) Vám bude k dispozici po celou dobu práce s creatorem.</p>
        </div>
        <h2>VYBRÁNO: <span id="ornavi_chosen_fav">0</span>/10</h2>
    </div>
    <div id="choose_fav_products">
    </div>
</div>

<div id="ornavi_filter_modal_click" style="display: none">
    <h2>Filtrovat podle:</h2>
    <hr>
    <table>
        {foreach from=$filterValues key=filterName item=filter}
            <tr>
                <th>{$filterValuesName[$filterName]}</th>
                <td>
                    <select class="ornavi_select" name="{$filterName}">
                            <option value="no" selected="selected" class="ornavi_attr">Vyberte možnost</option>
                        {foreach from=$filter item=filterVal}
                            <option value="{$filterVal}" class="ornavi_attr">{$filterVal}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
        {/foreach}
    </table>
    <hr>
    <div class="ornavi_filter_foot">
        <img src="{$moduleRelPath}views/img/logo.png"><span>filtr všech produktů</span>
    </div>
</div>
